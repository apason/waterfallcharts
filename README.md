### Waterfallcharts

This is a fork of the python package waterfallcharts (https://github.com/chrispaulca/waterfall). This package can be used to make waterfall charts easily. A few changes have been made to the original code to make the chart more suitable for us.

The package is meant to be used with the Bankruptcy risk SHAP figure project.